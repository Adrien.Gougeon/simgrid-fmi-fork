#include "simgrid-fmi.hpp"
#include "master.hpp"
#include <vector>
#include <unordered_map>
#include <simgrid/simix.hpp>

XBT_LOG_NEW_DEFAULT_SUBCATEGORY(surf_fmi, surf, "Logging specific to the SURF FMI plugin");


namespace simgrid{
namespace fmi{

/*
 * Signal triggered by the master when FMUs states change.
 */
xbt::signal<void()> on_state_change;

MasterFMI* master;

/**
 * FMIPlugin
 *
 */
void add_fmu_cs(const std::string& fmu_uri, const std::string& fmu_name, bool iterateAfterInput){
  master->add_fmu_cs(fmu_uri, fmu_name, iterateAfterInput);
}

void connect_fmu(const std::string& out_fmu_name, const std::string& output_port, const std::string& in_fmu_name, const std::string& input_port){
  master->connect_fmu(out_fmu_name,output_port,in_fmu_name,input_port);
}

void connect_real_to_simgrid(const std::function<double(std::vector<std::string>)>& generateInput,
                             const std::vector<std::string>& params, const std::string& fmu_name,
                             const std::string& input_name)
{
  master->connect_real_to_simgrid(generateInput,params,fmu_name,input_name);
}

void connect_integer_to_simgrid(const std::function<int(std::vector<std::string>)>& generateInput,
                                const std::vector<std::string>& params, const std::string& fmu_name,
                                const std::string& input_name)
{
  master->connect_integer_to_simgrid(generateInput,params,fmu_name,input_name);
}

void connect_boolean_to_simgrid(const std::function<bool(std::vector<std::string>)>& generateInput,
                                const std::vector<std::string>& params, const std::string& fmu_name,
                                const std::string& input_name)
{
  master->connect_boolean_to_simgrid(generateInput,params,fmu_name,input_name);
}

void connect_string_to_simgrid(const std::function<std::string(std::vector<std::string>)>& generateInput,
                               const std::vector<std::string>& params, const std::string& fmu_name,
                               const std::string& input_name)
{
  master->connect_string_to_simgrid(generateInput,params,fmu_name,input_name);
}

void init(double communication_step){
  if (master == nullptr)
    master = new MasterFMI(communication_step);
}

void ready_for_simulation(){
  //FIXME: this is ugly. the shared_ptr should be filled ASAP, not later like this.
  std::shared_ptr<MasterFMI> m(master);

  master->init_couplings();
  simgrid::s4u::Engine::get_instance()->add_model(m);
}

double get_real(const std::string& fmi_name, const std::string& output_name){
  return master->get_real(fmi_name, output_name, true);
}

bool get_boolean(const std::string& fmi_name, const std::string& output_name){
  return master->get_boolean(fmi_name, output_name, true);
}

int get_integer(const std::string& fmi_name, const std::string& output_name){
  return master->get_integer(fmi_name, output_name, true);
}

std::string get_string(const std::string& fmi_name, const std::string& output_name){
  return master->get_string(fmi_name, output_name, true);
}

var_type get_var_type(const std::string& fmu_name, const std::string& var_name){
  return master->get_var_type(fmu_name, var_name);
}

void set_real(const std::string& fmi_name, const std::string& input_name, double value){
  kernel::actor::simcall_answered(
      [fmi_name, input_name, value]() { master->set_real(fmi_name, input_name, value, true); });
}

void set_boolean(const std::string& fmi_name, const std::string& input_name, bool value){
  kernel::actor::simcall_answered(
      [fmi_name, input_name, value]() { master->set_boolean(fmi_name, input_name, value, true); });
}

void set_integer(const std::string& fmi_name, const std::string& input_name, int value){
  kernel::actor::simcall_answered(
      [fmi_name, input_name, value]() { master->set_integer(fmi_name, input_name, value, true); });
}

void set_string(const std::string& fmi_name, const std::string& input_name, const std::string& value){
  kernel::actor::simcall_answered(
      [fmi_name, input_name, value]() { master->set_string(fmi_name, input_name, value, true); });
}

void register_event(std::function<bool(const std::vector<std::string>&)> condition, std::function<void(const std::vector<std::string>&)> handleEvent, std::vector<std::string> params){
  kernel::actor::simcall_answered(
      [condition, handleEvent, params]() { master->register_event(condition, handleEvent, params); });
}

void delete_events(){
  master->delete_events();
}


/**
 * MasterFMI
 */

MasterFMI::MasterFMI(const double stepSize) : Model("MasterFMI"), commStep(stepSize)
{
  set_update_algorithm(kernel::resource::Model::UpdateAlgo::LAZY);
}

MasterFMI::~MasterFMI() {
  output.close();
}


void MasterFMI::add_fmu_cs(const std::string& fmu_uri, const std::string& fmu_name, bool iterateAfterInput){

  XBT_DEBUG("loading FMU %s at %s", fmu_uri.c_str(), fmu_name.c_str());

  const double start_time = s4u::Engine::get_clock();

  fmi4cpp::fmi2::fmu fmu(fmu_uri);
  auto cs_fmu = fmu.as_cs_fmu();
  fmus[fmu_name] = cs_fmu->new_instance();

  XBT_DEBUG("FMU-CS %s at %s is loaded", fmu_uri.c_str(), fmu_name.c_str());

  fmus[fmu_name]->setup_experiment(start_time);
  fmus[fmu_name]->enter_initialization_mode();
  fmus[fmu_name]->exit_initialization_mode();

  XBT_DEBUG("FMU-CS %s at %s is initialized", fmu_uri.c_str(), fmu_name.c_str());
}


void MasterFMI::connect_fmu(const std::string& out_fmu_name, const std::string& output_port, const std::string& in_fmu_name, const std::string& input_port){

  check_not_ready_for_simulation();
  check_port_validity(out_fmu_name,output_port,fmi4cpp::fmi2::UNKNOWN_TYPE,false);
  std::string out_type = fmus[out_fmu_name]->get_model_description()->get_variable_by_name(output_port).type_name();
  check_port_validity(in_fmu_name,input_port,out_type,true);

  port out{out_fmu_name, output_port};
  port in{in_fmu_name, input_port};
  in_coupled_input.push_back(in);
  couplings[in]=out;
}

void MasterFMI::connect_real_to_simgrid(const std::function<double(std::vector<std::string>)>& generateInput,
                                        const std::vector<std::string>& params, const std::string& fmu_name,
                                        const std::string& input_name)
{

  check_not_ready_for_simulation();
  check_port_validity(fmu_name,input_name,fmi4cpp::fmi2::REAL_TYPE,true);

  externalCoupling = true;

  port in{fmu_name, input_name};
  simgrid_fmu_connection<double> connection{in, generateInput, params};

  real_ext_couplings.push_back(connection);
  ext_coupled_input.push_back(in);
}

void MasterFMI::connect_integer_to_simgrid(const std::function<int(std::vector<std::string>)>& generateInput,
                                           const std::vector<std::string>& params, const std::string& fmu_name,
                                           const std::string& input_name)
{

  check_not_ready_for_simulation();
  check_port_validity(fmu_name,input_name,fmi4cpp::fmi2::INTEGER_TYPE,true);

  externalCoupling = true;

  port in{fmu_name, input_name};
  simgrid_fmu_connection<int> connection{in, generateInput, params};

  integer_ext_couplings.push_back(connection);
  ext_coupled_input.push_back(in);
}

void MasterFMI::connect_boolean_to_simgrid(const std::function<bool(std::vector<std::string>)>& generateInput,
                                           const std::vector<std::string>& params, const std::string& fmu_name,
                                           const std::string& input_name)
{

  check_not_ready_for_simulation();
  check_port_validity(fmu_name,input_name,fmi4cpp::fmi2::BOOLEAN_TYPE,true);

  externalCoupling = true;

  port in{fmu_name, input_name};
  simgrid_fmu_connection<bool> connection{in, generateInput, params};

  boolean_ext_couplings.push_back(connection);
  ext_coupled_input.push_back(in);
}

void MasterFMI::connect_string_to_simgrid(const std::function<std::string(std::vector<std::string>)>& generateInput,
                                          const std::vector<std::string>& params, const std::string& fmu_name,
                                          const std::string& input_name)
{

  check_not_ready_for_simulation();
  check_port_validity(fmu_name,input_name,fmi4cpp::fmi2::STRING_TYPE,true);

  externalCoupling = true;

  port in{fmu_name, input_name};
  simgrid_fmu_connection<std::string> connection{in, generateInput, params};

  string_ext_couplings.push_back(connection);
  ext_coupled_input.push_back(in);
}

double MasterFMI::get_real(const std::string& fmi_name, const std::string& output_name, bool checkPort){

  if(checkPort)
    check_port_validity(fmi_name,output_name,fmi4cpp::fmi2::REAL_TYPE,false);

  fmi2Real out;

  auto fmu_md = fmus[fmi_name]->get_model_description();
  auto var = fmu_md->get_variable_by_name(output_name).as_real(); 
  auto vr = var.valueReference();

  xbt_assert(fmus[fmi_name]->read_real(vr, out), "FMI %s failed to return the value of variable %s", fmi_name.c_str(),
             output_name.c_str());

  return (double) out;
}

bool MasterFMI::get_boolean(const std::string& fmi_name, const std::string& output_name, bool checkPort){

  if(checkPort)
    check_port_validity(fmi_name,output_name,fmi4cpp::fmi2::BOOLEAN_TYPE,false);

  fmi2Boolean out;

  auto fmu_md = fmus[fmi_name]->get_model_description();
  auto var = fmu_md->get_variable_by_name(output_name).as_boolean();
  auto vr = var.valueReference();

  xbt_assert(fmus[fmi_name]->read_boolean(vr, out), "FMI %s failed to return the value of variable %s",
             fmi_name.c_str(), output_name.c_str());

  return (bool) out;
}

int MasterFMI::get_integer(const std::string& fmi_name, const std::string& output_name, bool checkPort){

  if(checkPort)
    check_port_validity(fmi_name,output_name,fmi4cpp::fmi2::INTEGER_TYPE,false);

  fmi2Integer out;

  auto fmu_md = fmus[fmi_name]->get_model_description();
  auto var = fmu_md->get_variable_by_name(output_name).as_integer();
  auto vr = var.valueReference();

  xbt_assert(fmus[fmi_name]->read_integer(vr, out), "FMI %s failed to return the value of variable %s",
             fmi_name.c_str(), output_name.c_str());

  return (int) out;
}

std::string MasterFMI::get_string(const std::string& fmi_name, const std::string& output_name, bool checkPort){

  if(checkPort)
    check_port_validity(fmi_name,output_name,fmi4cpp::fmi2::STRING_TYPE,false);

  fmi2String out;

  auto fmu_md = fmus[fmi_name]->get_model_description();
  auto var = fmu_md->get_variable_by_name(output_name).as_string();
  auto vr = var.valueReference();

  xbt_assert(fmus[fmi_name]->read_string(vr, out), "FMI %s failed to return the value of variable %s", fmi_name.c_str(),
             output_name.c_str());

  return (std::string) out;
}

var_type MasterFMI::get_var_type(const std::string& fmu_name, const std::string& var_name){
  auto var = fmus[fmu_name]->get_model_description()->get_variable_by_name(var_name);
  if(var.is_real())
    return var_type::real; 
  if(var.is_boolean())
    return var_type::boolean;
  if(var.is_integer())
    return var_type::integer;
  if(var.is_string())
    return var_type::string;

  return var_type::unknown;
}

void MasterFMI::set_real(const std::string& fmi_name, const std::string& input_name, double value, bool simgrid_input){

  if(simgrid_input)
    check_port_validity(fmi_name,input_name,fmi4cpp::fmi2::REAL_TYPE,simgrid_input);

  auto fmu_md = fmus[fmi_name]->get_model_description();
  auto var = fmu_md->get_variable_by_name(input_name).as_real();
  auto vr = var.valueReference();

  xbt_assert(fmus[fmi_name]->write_real(vr, value), "FMU %s failed to set its port %s to value %f", fmi_name.c_str(),
             input_name.c_str(), value);

  if (iterate_input[fmi_name])
    xbt_assert(fmus[fmi_name]->step(0.),
               "FMU %s failed to perform a doStep(dt=0) after setting an input (you should may be set "
               "iterateAfterInput=false when adding the FMU CS).",
               fmi_name.c_str());

  if(simgrid_input && ready_for_simulation){
    solve_couplings(false);
    manage_event_notification();
  }
}

void MasterFMI::set_boolean(const std::string& fmi_name, const std::string& input_name, bool value, bool simgrid_input){

  if(simgrid_input)
    check_port_validity(fmi_name,input_name,fmi4cpp::fmi2::BOOLEAN_TYPE,simgrid_input);

  auto fmu_md = fmus[fmi_name]->get_model_description();
  auto var = fmu_md->get_variable_by_name(input_name).as_boolean();
  auto vr = var.valueReference();

  xbt_assert(fmus[fmi_name]->write_boolean(vr, value), "FMU %s failed to set its port %s to value %d", fmi_name.c_str(),
             input_name.c_str(), value);

  if (iterate_input[fmi_name])
    xbt_assert(fmus[fmi_name]->step(0.),
               "FMU %s failed to perform a doStep(dt=0) after setting an input (you should may be set "
               "iterateAfterInput=false when adding the FMU CS).",
               fmi_name.c_str());

  if(simgrid_input && ready_for_simulation){
    solve_couplings(false);
    manage_event_notification();
  }
}

void MasterFMI::set_integer(const std::string& fmi_name, const std::string& input_name, int value, bool simgrid_input){

  if(simgrid_input)
    check_port_validity(fmi_name,input_name,fmi4cpp::fmi2::INTEGER_TYPE,simgrid_input);

  auto fmu_md = fmus[fmi_name]->get_model_description();
  auto var = fmu_md->get_variable_by_name(input_name).as_integer();
  auto vr = var.valueReference();

  xbt_assert(fmus[fmi_name]->write_integer(vr, value), "FMU %s failed to set its port %s to value %d", fmi_name.c_str(),
             input_name.c_str(), value);

  if (iterate_input[fmi_name])
    xbt_assert(fmus[fmi_name]->step(0.),
               "FMU %s failed to perform a doStep(dt=0) after setting an input (you should may be set "
               "iterateAfterInput=false when adding the FMU CS).",
               fmi_name.c_str());

  if(simgrid_input && ready_for_simulation){
    solve_couplings(false);
    manage_event_notification();
  }
}

void MasterFMI::set_string(const std::string& fmi_name, const std::string& input_name, const std::string& value, bool simgrid_input){

  if(simgrid_input)
    check_port_validity(fmi_name,input_name, fmi4cpp::fmi2::STRING_TYPE, simgrid_input);

  fmi2String c_value = value.c_str();

  auto fmu_md = fmus[fmi_name]->get_model_description();
  auto var = fmu_md->get_variable_by_name(input_name).as_string();
  auto vr = var.valueReference();
  fmus[fmi_name]->write_string(vr, c_value);

  xbt_assert(fmus[fmi_name]->write_string(vr, c_value), "FMU %s failed to set its port %s to value %s",
             fmi_name.c_str(), input_name.c_str(), c_value);

  if (iterate_input[fmi_name])
    xbt_assert(fmus[fmi_name]->step(0.),
               "FMU %s failed to perform a doStep(dt=0) after setting an input (you should may be set "
               "iterateAfterInput=false when adding the FMU CS).",
               fmi_name.c_str());

  if(simgrid_input && ready_for_simulation){
    solve_couplings(false);
    manage_event_notification();
  }
}

void MasterFMI::solve_couplings(bool firstIteration){

  bool change = true;
  int i = 0;
  while(change){
    change = false;
    for(port in : in_coupled_input){
      change = (solve_coupling(in, couplings[in],!firstIteration) || change);
    }
    if(firstIteration)
      firstIteration = false;
    i++;
  }

  on_state_change();
}

bool MasterFMI::solve_coupling(const port& in, const port& out, bool checkChange){

  bool change = false;

  auto var = fmus[out.fmu]->get_model_description()->get_variable_by_name(out.name);
  if(var.is_real()){
    double r_out = get_real(out.fmu, out.name);
    if( !checkChange || r_out !=  last_real_outputs[out]){
      set_real(in.fmu, in.name, r_out,false);
      last_real_outputs[out] = r_out;
      change = true;
    }

  }else if(var.is_integer()){
    int i_out = get_integer(out.fmu, out.name);
    if( !checkChange || i_out != last_int_outputs[out]){
      set_integer(in.fmu, in.name, i_out,false);
      last_int_outputs[out] = i_out;
      change = true;
    }
  }

  else if(var.is_boolean()){
    bool b_out = get_boolean(out.fmu, out.name);
    if( !checkChange || b_out != last_bool_outputs[out]){
      set_boolean(in.fmu, in.name, b_out,false);
      last_bool_outputs[out] = b_out;
      change = true;
    }

  }else if(var.is_string()){
    std::string s_out = get_string(out.fmu, out.name);
    if( !checkChange || s_out != last_string_outputs[out]){
      set_string(in.fmu, in.name, s_out,false);
      last_string_outputs[out] = s_out;
      change = true;
    }
  }

  return change;
}

void MasterFMI::solve_external_couplings(){

  for (auto coupling : real_ext_couplings) {
    double input = coupling.generateInput(coupling.params);
    set_real(coupling.in.fmu, coupling.in.name, input,false);
  }

  for (auto coupling : integer_ext_couplings) {
    int input = coupling.generateInput(coupling.params);
    set_integer(coupling.in.fmu, coupling.in.name, input,false);
  }

  for (auto coupling : boolean_ext_couplings) {
    bool input = coupling.generateInput(coupling.params);
    set_boolean(coupling.in.fmu, coupling.in.name, input,false);
  }

  for (auto coupling : string_ext_couplings) {
    std::string input = coupling.generateInput(coupling.params);
    set_string(coupling.in.fmu, coupling.in.name, input,false);
  }
}


void MasterFMI::update_actions_state(double now, double delta){

  XBT_DEBUG("updating the FMUs at time = %f, delta = %f",now,delta);
  
  while(current_time < now){
    double dt = std::min(commStep, now - current_time);
    XBT_DEBUG("current_time = %f perform doStep of %f ",current_time, dt);
    for (auto& it : fmus)
      xbt_assert(it.second->step(dt), "FMU %s failed to go from time %f to time %f during the co-simulation",
                 it.first.c_str(), current_time, (current_time + dt));

    current_time += dt;
    if(current_time != now){
      solve_couplings(true);
    }
  }

  solve_external_couplings();
  solve_couplings(true);
  manage_event_notification();  
}

void MasterFMI::init_couplings(){
  ready_for_simulation = true;
  solve_external_couplings();
  solve_couplings(true);
  manage_event_notification();
}

double MasterFMI::next_occurring_event(double now){

  // we first solve the external couplings if some of the model states have been changed by the actors
  if(externalCoupling){
    solve_external_couplings();
    solve_couplings(true); // We may need to re-propagate the results, e.g. if actors generated some instantaneous events
  }

  if(firstEvent || check_event_occurence()){
    firstEvent = false;
    return 0;
  } else if (event_handlers.empty()) {
    return -1;
  } else {
    return commStep;
  }
}


void MasterFMI::register_event(std::function<bool(const std::vector<std::string>&)> condition, std::function<void(const std::vector<std::string>&)> handleEvent, std::vector<std::string> handlerParam)
{
  if(condition(handlerParam)){
    handleEvent(handlerParam);
  }else{
    event_handlers.push_back(handleEvent);
    event_conditions.push_back(condition);
    event_params.push_back(handlerParam);
  }
}

bool MasterFMI::check_event_occurence(){
  for (unsigned int i = 0; i < event_handlers.size(); i++) {
    if (event_conditions[i](event_params[i]))
      return true;
  }
  return false;
}

void MasterFMI::manage_event_notification(){
  unsigned int i = 0;
  while (i < event_handlers.size()) {
    bool isEvent = event_conditions[i](event_params[i]);
    if(isEvent){
      event_conditions.erase(event_conditions.begin()+i);
      auto handleEvent = event_handlers[i];
      event_handlers.erase(event_handlers.begin()+i);
      std::vector<std::string> handlerParam = event_params[i];
      event_params.erase(event_params.begin()+i);

      handleEvent(handlerParam);
    }
    else 
      i++;
  }
}

void MasterFMI::delete_events(){
  event_handlers.clear();
  event_conditions.clear();
  event_params.clear();
}

bool MasterFMI::is_input_coupled(std::string fmu, std::string input_name){
  port input{fmu, input_name};
  return std::find(in_coupled_input.begin(), in_coupled_input.end(), input) != in_coupled_input.end()
      || std::find(ext_coupled_input.begin(), ext_coupled_input.end(), input) != ext_coupled_input.end();
}

void MasterFMI::check_port_validity(std::string fmu_name, std::string port_name, std::string type, bool check_already_coupled){

  xbt_assert(fmus.find(fmu_name) != fmus.end(), "unknown FMU %s", fmu_name.c_str());

  std::string var_type = fmus[fmu_name]->get_model_description()->get_variable_by_name(port_name).type_name();

  xbt_assert(type == fmi4cpp::fmi2::UNKNOWN_TYPE || var_type == type, "wrong type compatibility for port %s of FMU %s.",
             port_name.c_str(), fmu_name.c_str());

  xbt_assert(not check_already_coupled || not is_input_coupled(fmu_name, port_name),
             "port %s of FMU %s is already coupled to a model", port_name.c_str(), fmu_name.c_str());
}

void MasterFMI::check_not_ready_for_simulation(){
  xbt_assert(not ready_for_simulation,
             "You cannot modify the FMI model after calling simgrid::fmi::ready_for_simulation().");
}

}
}
