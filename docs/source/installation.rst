.. _install:

Installing SimGrid-FMI
======================

Installation
------------

SimGrid-FMI should work on Linux and Windows (under Windows, you need to install the Windows Subsystem Linux).

First, you need to install `SimGrid <https://simgrid.org/>`_ and `FMI4cpp <https://github.com/NTNU-IHB/FMI4cpp>`_.

Then, get SimGrid-FMI from its `framagit repository <https://framagit.org/simgrid/simgrid-FMI>`_, and compile it as follows:

.. code-block:: console

    $ git clone https://framagit.org/simgrid/simgrid-FMI.git
    $ cd simgrid-FMI
    $ cmake .
    $ make install


Testing your build
------------------

You can run and test several examples that use FMUs exported with OpenModelica. But first, you need to install OpenModelica and export all the FMUs, with the following commands:

.. code-block:: console

    $ echo "deb https://build.openmodelica.org/apt bullseye release" | tee /etc/apt/sources.list.d/openmodelica.list
    $ wget -q http://build.openmodelica.org/apt/openmodelica.asc -O- | apt-key add -
    $ apt update
    $ apt install -y omc
    $ ./examples/buildFMU.sh
    $ ctest



