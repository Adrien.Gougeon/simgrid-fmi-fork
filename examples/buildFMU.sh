#! /bin/sh
set -x
cd examples
omc buildFMU.mos

cd pandapower/pandapower-fmu
python3 makeFMU.py -i ../minimal_example/resources/input_ports.txt -o ../minimal_example/resources/output_ports.txt -nw ../minimal_example/resources/network.json
mv pandapower.fmu ../minimal_example

python3 makeFMU.py -i ../cigre_network/resources/input_ports.txt -o ../cigre_network/resources/output_ports.txt -nw ../cigre_network/resources/network.json
mv pandapower.fmu ../cigre_network

