model monolithic_lotka_volterra

parameter Real alpha = 2/3;
parameter Real beta = 4/3;
parameter Real gamma = 1;
parameter Real delta = 1;

output Real x, y;

initial equation
  x = 1;
  y = 1;

equation
  der(x) = alpha * x - beta * x * y;
  der(y) = delta * x * y - gamma * y;

end monolithic_lotka_volterra;


model lotka_volterra_x

parameter Real alpha = 2/3;
parameter Real beta = 4/3;

input Real y;
output Real x;

initial equation
 x = 1;

equation
 der(x) = alpha * x - beta * x * y;

end lotka_volterra_x;



model lotka_volterra_y

parameter Real gamma = 1;
parameter Real delta = 1;

output Real y;
input Real x;

initial equation
 y = 1;

equation
 der(y) = delta * x * y - gamma * y;

end lotka_volterra_y;

